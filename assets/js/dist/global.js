(function () {
'use strict';

var helper = {
    breakpoint: {
        // 'small': 0,
        // 'medium': 640,
        // 'large': 1025,
        // 'xlarge': 1300,
        // 'xxlarge': 1500,
    },
    touch_support: Modernizr.touch
};

var wait_for_final_event = function () {

    var timers = {};

    return function (callback, ms, uniqueId) {
        if (!uniqueId) {
            // don't call this twice without a uniqueID
            uniqueId = Math.random() * 1000;
        }
        if (timers[uniqueId]) {
            clearTimeout(timers[uniqueId]);
        }
        timers[uniqueId] = setTimeout(callback, ms);
    };
}();

// Plugin Functions

var $ = jQuery.noConflict();

var plugins = {
    init: function init() {

        // Foundation must go first as it is a dependancy
        this.foundation.init();

        this.lazy_sizes.init();
        this.match_height.init();
        this.inview.init();
    },


    foundation: {
        init: function init() {

            $(document).foundation();
        }
    },

    lazy_sizes: {
        init: function init() {

            window.lazySizesConfig = window.lazySizesConfig || {};
            lazySizesConfig.expand = 1500;
            lazySizesConfig.loadHidden = false;
        },
        load: function load() {}
    },

    match_height: {

        init: function init() {

            $('.js-match-height').matchHeight();
        }

    },

    inview: {

        init: function init() {

            if ($('.js-animate').length > 0) {

                $('.js-animate').on('inview', function (event, isInView) {

                    if (isInView) {

                        if (!$(this).hasClass('is-inview')) {
                            $(this).addClass('is-inview');
                        }
                    }
                });
            }
        }
    }
};

// Plugin Functions

var $$1 = jQuery.noConflict();

var slideshow = {
    init: function init() {

        this.slideshow.init();
    },


    slideshow: {
        init: function init() {

            $$1('.c-carousel__slider').flickity({
                // options
                cellAlign: 'left',
                contain: true,
                wrapAround: true,
                accessibility: false
            });
        }
    }
};

// Slideshow Functions

var $$2 = jQuery.noConflict();

var scroll_lock = {
    init: function init() {

        this.scroll_lock.init();
    },


    scroll_lock: {
        init: function init() {},
        lock: function lock() {
            var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;


            $$2('.site').css('top', -$$2(window).scrollTop());
            $$2('html').addClass('scroll-lock');

            if (!callback) return;

            callback();
        },
        unlock: function unlock() {

            if ($$2('html').hasClass('scroll-lock')) {

                var reset_scroll = parseInt($$2('.site').css('top')) * -1;

                $$2('html').removeClass('scroll-lock');
                $$2('.site').css('top', 'auto');
                $$2('html, body').animate({
                    scrollTop: reset_scroll
                }, 0);
            }
        }
    }
};

// Class import
// import Example from './examples';
// Var import
(function ($, document) {

    $(document).ready(function () {

        app_init.ready();
    });

    $(window).load(function () {

        slideshow.init();
    });

    $(window).scroll(function () {});

    $(window).resize(function () {

        wait_for_final_event(function () {}, 300, 'init');
    });

    var app_init = {

        ready: function ready() {

            // Scroll Lock
            scroll_lock.init();

            // Plugins
            plugins.init();

            // Slideshow
            slideshow.init();

            // App
            app.init();
        }

        // App Functions

    };var app = {

        init: function init() {

            this.scroll_to.init();
            this.sticky.init();
            this.parallax.init();
            this.video.init();
            this.email_signup.init();
        },

        scroll_to: {
            init: function init() {

                // $('.c-hero__logo .c-button').on('click', function() {

                //     $('html, body').animate({
                //         scrollTop: $('.js-scroll-to').offset().top
                //     }, 500);

                //     return false;
                // });

            }
        },

        sticky: {
            init: function init() {

                // $('.c-hero__logo .c-button').sticky({topSpacing:70});

                // $('.c-cta').sticky({topSpacing:0});

            }
        },

        parallax: {
            init: function init() {

                // var $window = $(window);

                // $('[data-parallax]').each(function(){

                //     var $this = $(this);

                //     $window.scroll(function() {

                //         var thisPos = $this.offset().top;
                //         var yPos = -($window.scrollTop() - thisPos ) * 0.09 ;
                //         var coords = yPos;

                //         $this.css({ 'transform' : 'translateY(' + yPos + 'px)' });

                //     });

                // });

                // $('[data-parallax-image]').each(function(){

                //     var $this = $(this);

                //     $window.scroll(function() {

                //         var thisPos = $this.offset().top;
                //         var yPos = -($window.scrollTop() - thisPos ) * 0.04 ;
                //         var coords = yPos - (yPos * 2);

                //         $this.css({ 'transform' : 'translateY(' + coords + 'px) scale(1.1)' });

                //     });

                // });

            }
        },

        video: {
            init: function init() {

                // $('.c-hero__button').on('click', function() {
                //     if( $(this).hasClass('is-paused') ) {
                //         $('.c-hero__video video').trigger('play');
                //         $(this).removeClass('is-paused');
                //     } else {
                //         $('.c-hero__video video').trigger('pause');
                //         $(this).addClass('is-paused');
                //     }
                //     return false;
                // });

            }
        },

        email_signup: {
            init: function init() {

                // function validateEmail($email) {
                //     var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                //     return emailReg.test( $email );
                // }

                // $('.c-newsletter-signup').on('submit', function(){

                //     var emailaddress = $('.c-newsletter-signup__text').val();

                //     if( !validateEmail(emailaddress)) {

                //         $('.c-newsletter-signup').addClass('c-newsletter-signup--error');

                //         return false;

                //     } else {

                //         $('.c-newsletter-signup').removeClass('c-newsletter-signup--error');

                //     }

                // });

            }
        }
    };
})(jQuery, document);

}());
