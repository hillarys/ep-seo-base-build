// Class import
// import Example from './examples';
// Var import
import { wait_for_final_event } from './helper';
import { plugins } from './plugins';
import { slideshow } from './slideshow';
import { scroll_lock } from './scroll_lock';

(function( $, document ) {

    $( document ).ready( function() {

        app_init.ready();

    });

    $( window ).load( function() {

        slideshow.init();
    });

    $( window ).scroll( function() {
    });

    $( window ).resize( function() {

        wait_for_final_event( function() {

        }, 300, 'init');

    });

    const app_init = {

        ready : function() {

            // Scroll Lock
            scroll_lock.init();

            // Plugins
            plugins.init();

            // Slideshow
            slideshow.init();

            // App
            app.init();

        }
    }

    // App Functions

    let app = {

        init : function() {

            this.scroll_to.init();
            this.sticky.init();
            this.parallax.init();
            this.video.init();
            this.email_signup.init();

        },

        scroll_to : {

            init () {

                // $('.c-hero__logo .c-button').on('click', function() {

                //     $('html, body').animate({
                //         scrollTop: $('.js-scroll-to').offset().top
                //     }, 500);

                //     return false;
                // });

            }

        },

        sticky : {

            init () {

                // $('.c-hero__logo .c-button').sticky({topSpacing:70});

                // $('.c-cta').sticky({topSpacing:0});

            }

        },

        parallax : {

            init () {

                // var $window = $(window);

                // $('[data-parallax]').each(function(){

                //     var $this = $(this);

                //     $window.scroll(function() {

                //         var thisPos = $this.offset().top;
                //         var yPos = -($window.scrollTop() - thisPos ) * 0.09 ;
                //         var coords = yPos;

                //         $this.css({ 'transform' : 'translateY(' + yPos + 'px)' });

                //     });

                // });

                // $('[data-parallax-image]').each(function(){

                //     var $this = $(this);

                //     $window.scroll(function() {

                //         var thisPos = $this.offset().top;
                //         var yPos = -($window.scrollTop() - thisPos ) * 0.04 ;
                //         var coords = yPos - (yPos * 2);

                //         $this.css({ 'transform' : 'translateY(' + coords + 'px) scale(1.1)' });

                //     });

                // });

            }

        },

        video : {

            init () {

                // $('.c-hero__button').on('click', function() {
                //     if( $(this).hasClass('is-paused') ) {
                //         $('.c-hero__video video').trigger('play');
                //         $(this).removeClass('is-paused');
                //     } else {
                //         $('.c-hero__video video').trigger('pause');
                //         $(this).addClass('is-paused');
                //     }
                //     return false;
                // });

            }

        },

        email_signup : {

            init () {

                // function validateEmail($email) {
                //     var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                //     return emailReg.test( $email );
                // }

                // $('.c-newsletter-signup').on('submit', function(){

                //     var emailaddress = $('.c-newsletter-signup__text').val();

                //     if( !validateEmail(emailaddress)) {

                //         $('.c-newsletter-signup').addClass('c-newsletter-signup--error');

                //         return false;

                //     } else {

                //         $('.c-newsletter-signup').removeClass('c-newsletter-signup--error');

                //     }

                // });

            }
        }
    }

}(jQuery, document));