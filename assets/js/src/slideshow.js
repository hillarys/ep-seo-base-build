export { slideshow };

// Plugin Functions

let $ = jQuery.noConflict();

let slideshow = {

    init () {

        this.slideshow.init();

    },

    slideshow : {

        init () {

            $('.c-carousel__slider').flickity({
                // options
                cellAlign: 'left',
                contain: true,
                wrapAround: true,
                accessibility: false
            });

        }

    }
}