<footer class="footer">
        <div class="trust-message__wrapper">
          <div class="trust-message__inner">
            <p class="col col--1-of-4--md trust-message__item">
              <span class="trust-message__item__title">Stylish range and expert advice</span>
              <span class="trust-message__item__text">For a perfect finish every time</span>
            </p>
            <p class="col col--1-of-4--md trust-message__item u-display--none-small">
              <span class="trust-message__item__title">All the latest designs and colours</span>
              <span class="trust-message__item__text">You’ll find exactly what you’re looking for</span>
            </p>
            <p class="col col--1-of-4--md trust-message__item u-display--none-small">
              <span class="trust-message__item__title">Everything’s guaranteed</span>
              <span class="trust-message__item__text">For total peace of mind</span>
            </p>
            <p class="col col--1-of-4--md trust-message__item u-display--none-small">
              <span class="trust-message__item__title">In-home service</span>
              <span class="trust-message__item__text">We make it easy to choose what’s right</span>
            </p>
          </div>
        </div>
        
                <nav class="footer__nav clearfix">
                <div class="footer__inner">
                  <h2 class="u-visuallyhidden">Footer navigation</h2>
                    <div class="col col--1-of-4--md newsletter-signup">
          <form class="newsletter-signup__form" method="POST" action="/newsletter-signup-form/" data-form-validation="" novalidate="true">
            <fieldset class="input__fieldset">
              <legend class="input__label">Newsletter Signup</legend>
              <div class="input__row">
                <label class="input__label input__label-hint newsletter-signup__text" for="NewsletterEmailAddress">Sign up to receive our latest offers and deals. Enter your email address below:</label>
                <div class="input__holder">
                  <input type="text" class="input__text" name="NewsletterEmailAddress" id="NewsletterEmailAddress" data-validate="email" required="" data-hj-suppress="">
                </div>
                <span class="input__error-message">Please enter a valid email address</span>
              </div>
              <button type="submit" class="input__submit" data-form-submit="">Sign Up</button>
            </fieldset>
          </form>
        </div>
        
        <div class="col col--1-of-2 col--1-of-4--md footer__nav__links">
            <p class="footer__nav__heading">Hillarys</p>
            <ul class="list-stack secondary-links">
                <li class="list-stack__item">
                    <a href="/about-hillarys/your-local-hillarys/" class="list-stack__link secondary-links__link">Find your local advisor</a>
                </li>
                <li class="list-stack__item">
                    <a href="/careers/" class="list-stack__link secondary-links__link">Careers</a>
                </li>
                <li class="list-stack__item">
                    <a href="/sitemap/" class="list-stack__link secondary-links__link">Sitemap</a>
                </li>
                <li class="list-stack__item">
                    <a href="/privacy-policy/" class="list-stack__link secondary-links__link">Privacy Policy</a>
                </li>
                <li class="list-stack__item">
                    <a href="/careers/candidate-privacy-policy/" class="list-stack__link secondary-links__link">Candidate privacy policy</a>
                </li>
                <li class="list-stack__item">
                    <a href="/terms-conditions/" class="list-stack__link secondary-links__link">Terms &amp; conditions</a>
                </li>
                <li class="list-stack__item">
                    <a href="/anti-slavery/" class="list-stack__link secondary-links__link">Anti-slavery</a>
                </li>
                <li class="list-stack__item">
                    <a href="/tax-strategy/" class="list-stack__link secondary-links__link">Tax strategy</a>
                </li>
                <li class="list-stack__item">
                    <a href="/gender-pay-gap/" class="list-stack__link secondary-links__link">Gender pay gap</a>
                </li>
                <li class="list-stack__item">
                    <a href="/cookie-information/" class="list-stack__link secondary-links__link">Cookie information</a>
                </li>
                <li class="list-stack__item">
                    <a href="//www.hillarys.ie" class="list-stack__link secondary-links__link">Hillarys Ireland</a>
                </li>
            </ul>
        </div>
        <div class="col col--1-of-2 col--1-of-4--md footer__nav__links">
            <p class="footer__nav__heading">Our range</p>
            <ul class="list-stack secondary-links">
                <li class="list-stack__item">
                    <a href="/blinds-range/" class="list-stack__link secondary-links__link">Blinds</a>
                </li>
                <li class="list-stack__item">
                    <a href="/shutters-range/" class="list-stack__link secondary-links__link">Shutters</a>
                </li>
                <li class="list-stack__item">
                    <a href="/curtains/" class="list-stack__link secondary-links__link">Curtains</a>
                </li>
                <li class="list-stack__item">
                    <a href="/conservatory-blinds/" class="list-stack__link secondary-links__link">Conservatory blinds</a>
                </li>
                <li class="list-stack__item">
                    <a href="/awnings/" class="list-stack__link secondary-links__link">Awnings</a>
                </li>
                <li class="list-stack__item">
                    <a href="/blinds-range/commercial-blinds/" class="list-stack__link secondary-links__link">Commercial Blinds</a>
                </li>
            </ul>
        </div>
        <div class="col col--1-of-2 col--1-of-4--md footer__nav__links">
            <p class="footer__nav__heading">Customer service</p>
            <ul class="list-stack secondary-links">
                <li class="list-stack__item">
                    <a href="/contact-us/" class="list-stack__link secondary-links__link">Contact Us</a>
                </li>
                <li class="list-stack__item">
                    <a href="/about-hillarys/faqs/" class="list-stack__link secondary-links__link">FAQs</a>
                </li>
                <li class="list-stack__item">
                    <a href="/inspiration/how-to-clean-blinds/" class="list-stack__link secondary-links__link">Cleaning your blinds</a>
                </li>
                <li class="list-stack__item">
                    <a href="/blinds-range/child-safe-blinds/" class="list-stack__link secondary-links__link">Child Safe Blinds</a>
                </li>
                <li class="list-stack__item">
                    <a href="/about-hillarys/product-guarantees/" class="list-stack__link secondary-links__link">Product Guarantees</a>
                </li>
                <li class="list-stack__item">
                    <a href="/arrange-an-appointment/" class="list-stack__link secondary-links__link">Arrange an Appointment</a>
                </li>
                <li class="list-stack__item">
                    <a href="/request-a-brochure/" class="list-stack__link secondary-links__link">Request a Hillarys Brochure</a>
                </li>
            </ul>
        </div>
        
                  </div>
                </nav>
        
        <nav class="social-strip social-strip--inner" data-social-component="component - social strip">
          <p class="social-strip__heading">Follow hillarys on:</p>
          <div class="social-strip__items">
            <span itemscope="" itemtype="https://schema.org/Organization">
              <link itemprop="url" href="https://www.hillarys.co.uk/">
              <a title="Find us on Facebook" itemprop="sameAs" class="social-strip__item" href="https://www.facebook.com/LoveHillarys" target="_blank" rel="noopener" data-social-action="follow" data-social-network="facebook">
                <svg class="social-strip__icon" width="18" height="18">
                  <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#facebook"></use>
                </svg>
                <span class="social-strip__body">Facebook</span>
              </a>
              <a title="Follow Hillarys on Twitter" itemprop="sameAs" class="social-strip__item" href="https://twitter.com/hillarysblinds" target="_blank" rel="noopener" data-social-action="follow" data-social-network="twitter">
                <svg class="social-strip__icon" width="18" height="18">
                  <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#twitter"></use>
                </svg>
                <span class="social-strip__body">Twitter</span>
              </a>
              <a title="Follow Hillarys on Pinterest" itemprop="sameAs" class="social-strip__item" href="https://www.pinterest.com/hillarysblinds/" target="_blank" rel="noopener" data-social-action="follow" data-social-network="pintrest">
                <svg class="social-strip__icon" width="18" height="18">
                  <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#pinterest"></use>
                </svg>
                <span class="social-strip__body">Pinterest</span>
              </a>
              <a title="Follow Hillarys on YouTube" itemprop="sameAs" class="social-strip__item" href="https://www.youtube.com/user/Hillarysblinds" target="_blank" rel="noopener" data-social-action="follow" data-social-network="youtube">
                <svg class="social-strip__icon" width="18" height="18">
                  <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#youtube"></use>
                </svg>
                <span class="social-strip__body">Youtube</span>
              </a>
              <a title="Follow Hillarys on instagram" itemprop="sameAs" class="social-strip__item" href="https://www.instagram.com/hillarysblinds/" target="_blank" rel="noopener" data-social-action="follow" data-social-network="instagram">
                <svg class="social-strip__icon" width="18" height="18">
                  <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#instagram"></use>
                </svg>
                <span class="social-strip__body">Instagram</span>
              </a>
              <a itemprop="sameAs" class="social-strip__item" href="https://www.houzz.co.uk/pro/hillarysblinds/hillarys/" data-social-action="follow" data-social-network="houzz">
                <img alt="Follow Hillarys on Houzz" class="social-strip__icon lazyloaded" data-src="//static.hillarys.co.uk/build/global/houzz.png" src="//static.hillarys.co.uk/build/global/houzz.png">
                <span class="social-strip__body">Houzz</span>
              </a>
            </span>
          </div>
        </nav>
        
              <div class="o-wrapper copyright-strip__wrapper">
                <div class="o-wrapper__inner copyright-strip">
                  <p class="copyright-strip">
                    All Rights Reserved © Copyright 2019 Hillarys. Made with
                    <a class="copyright-strip__link" href="https://www.codecomputerlove.com" rel="nofollow"> Computerlove</a>
                  </p>
                </div>
              </div>
        
    </footer>