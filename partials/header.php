<header class="site-header" data-mega-nav="">
        <div class="trust-message-strip" data-ga-category="component - trust message">
      <div class="trust-message-strip__wrapper">
        <ul class="trust-message-strip__trust-messages">
          <li class="trust-message-strip__trust-message">
            <a class="trust-message-strip__link" href="/about-hillarys/our-service/" data-ga-action="1" data-ga-label="Measuring &amp; fitting included">
              <svg class="trust-message-strip__icon" width="24" height="24">
                <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#tape-measure"></use>
              </svg>
              Measuring &amp; fitting included
            </a>
          </li>
          <li class="trust-message-strip__trust-message">
            <a class="trust-message-strip__link" href="/about-hillarys/product-testing/" data-ga-action="2" data-ga-label="Guaranteed for peace of mind">
              <svg class="trust-message-strip__icon" width="24" height="24">
                <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#guarantee"></use>
              </svg>
              Guaranteed for peace of mind
            </a>
          </li>
          <li class="trust-message-strip__trust-message">
            <a class="trust-message-strip__link" href="/about-hillarys/history-of-hillarys/" data-ga-action="3" data-ga-label="45 years’ experience">
              <svg class="trust-message-strip__icon" width="24" height="24">
                <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#calendar"></use>
              </svg>
              45 years’ experience
            </a>
          </li>
          <li class="trust-message-strip__trust-message trust-message-strip__trust-message--homes-on-four">
            <a class="trust-message-strip__link" href="/homeson4/" data-ga-action="4" data-ga-label="Exclusive collections">
              <svg class="trust-message-strip__icon trust-message-strip__icon--homes-on-four" width="177" height="32">
                <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#homes-on-four"></use>
              </svg>
              <span class="trust-message-strip__trust-message--hidden ">Sponsors of homes on 4</span>
            </a>
          </li>
        </ul>
      </div>
    </div>

  <div class="container site-header__wrapper site-header__wrapper--contains-sticky-call-now clearfix">
    

    <div class="site-header__call-now-wrapper site-header__call-now-wrapper--sticky">
      <a href="/arrange-an-appointment/" class="site-header__call-now">
        Request an appointment
        <svg class="styleguide__button-icon" viewBox="0 0 32 32" width="32" height="32">
          <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#arrow"></use>
        </svg>
      </a>
    </div>    


    <div class="site-header__desktop-wrapper" data-ga-category="component - header">
      <a class="site-header__logo" href="https://www.hillarys.co.uk" aria-label="Hillarys - You'll love what we do" itemscope="" itemtype="https://schema.org/Organization">
        <svg class="site-header__logo-icon">
          <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#logo"></use>
        </svg>
      </a>

      <div class="site-header__navigation-buttons">
          <span class="site-header__button">
            Call us
            <svg width="25" height="25" class="site-header__nav-icon site-header__nav-icon--call-us">
              <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#phone"></use>
            </svg>
            <a class="site-header__nav-text site-header__nav-text--hidden InfinityNumber" href="tel:08082397598" data-ga-action="nav button call us" data-ga-label="click to call">0808 239 7598</a>
          </span>

        <a class="site-header__button" href="/how-to-order-samples/" data-modal-toggle="samples-basket">
          Samples
          <svg class="site-header__nav-icon site-header__nav-icon--samples">
            <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#samples"></use>
          </svg>
          <span class="site-header__nav-text" data-sample-basket-total=""></span>
        </a>

        <button class="site-header__button" data-search-toggle="">
          Search

          <svg class="site-header__nav-icon" aria-hidden="true">
            <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#search"></use>
          </svg>
        </button>
      </div>

      
  <div class="cta-block">
    <div>
      <p class="body cta-block__text">
        <span class="cta-block__call-text">Call to request a free appointment</span>
          <span class="cta-block__number InfinityNumber">0808 239 7598</span>
        <span class="u-visuallyhidden">or</span>
        <a href="/contact-us/" class="cta-block__link">Contact us</a>
        <!-- <a href="/inspiration/" class="cta-block__link cta-block__link--tablet-only">Inspiration</a> -->
        <a href="/about-hillarys/" class="cta-block__link cta-block__link--tablet-only">About Us</a>
      </p>
    </div>
    <div class="cta-block__buttons">
      <a href="/arrange-an-appointment/" class="cta-block__request-an-appointment" data-ga-action="request an appointment" data-ga-label="request an appointment">Request an appointment online</a>
      <a href="/request-a-brochure/" class="cta-block__request-a-brochure" data-ga-action="request an brochure" data-ga-label="request a brochure">Request a brochure</a>
      <a href="/how-to-order-samples/" data-modal-toggle="samples-basket" class="cta-block__samples-btn" data-ga-action="samples" data-ga-label="open basket">
        <span class="cta-block__samples-text">My Samples</span>
        <span class="cta-block__samples-icon" data-sample-basket-total=""></span>
      </a>
    </div>
  </div>

    </div>
  </div>
  

<nav class="site-navigation" data-ga-category="component - mega nav" data-site-nav="">
  <div class="site-navigation__nav-bg" data-site-nav-bg="" style="height: 0px;"></div>
  <div class="site-navigation__header">
    <p class="site-navigation__title" data-menu-title="">Menu</p>
    <button class="site-navigation__mobile-header-button site-navigation__mobile-header-button--back site-navigation__mobile-header-button--hidden" aria-label="Go back to main menu" data-menu-back=""></button>
    <button class="site-navigation__mobile-header-button site-navigation__mobile-header-button--close" aria-label="Close menu" data-menu-toggle=""></button>
  </div>
  <button class="site-navigation__close-button-tablet icon--close--white" aria-label="Close menu" data-menu-toggle=""></button>
  <ul class="site-navigation__list o-wrapper__inner" itemscope="" itemtype="http://www.schema.org/SiteNavigationElement">
    <li class="site-navigation__item site-navigation__item--first-level">
      <a href="/" class="body site-navigation__link" itemprop="url" data-site-nav-first-level="" data-ga-action="home" data-ga-label="primary">
        <span itemprop="name">Home</span>
      </a>
    </li>

      <li class="site-navigation__item site-navigation__item--first-level">
        <a href="/blinds-range/" itemprop="url" class="body site-navigation__link site-navigation__link--has-child" data-ga-action="blinds" data-ga-label="primary" data-site-nav-first-level="">
          <span itemprop="name">Blinds</span>
        </a>
        <div class="site-navigation__child-wrapper">
          <ul class="site-navigation__child  site-navigation__child--second-level ">
            <li class="site-navigation__item">
              <a href="/blinds-range/" itemprop="url" class="site-navigation__link--large icon--arrow-right--brown" data-ga-action="blinds" data-ga-label="browse all blinds">
                Browse All Blinds
              </a>
            </li>
              <li class="site-navigation__item site-navigation__item--haschild">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="blinds" data-ga-label="view - our blinds range">
                  Our blinds range
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/blinds-range/roller-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="roller blinds">
                          <span itemprop="name">Roller blinds</span>
                          <span class="site-navigation__product-icon icon icon--roller"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/roman-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="roman blinds">
                          <span itemprop="name">Roman blinds</span>
                          <span class="site-navigation__product-icon icon icon--roman"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/blackout-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="blackout blinds">
                          <span itemprop="name">Blackout blinds</span>
                          <span class="site-navigation__product-icon icon icon--blackout-blind"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/wooden-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="wooden blinds">
                          <span itemprop="name">Wooden blinds</span>
                          <span class="site-navigation__product-icon icon icon--wooden"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/venetian-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="venetian blinds">
                          <span itemprop="name">Venetian blinds</span>
                          <span class="site-navigation__product-icon icon icon--venetian"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/pleated-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="pleated blinds">
                          <span itemprop="name">Pleated blinds</span>
                          <span class="site-navigation__product-icon icon icon--pleated"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/perfect-fit-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="perfect fit blinds">
                          <span itemprop="name">Perfect fit blinds</span>
                          <span class="site-navigation__product-icon icon icon--perfectfit"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/skylight-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="skylight blinds">
                          <span itemprop="name">Skylight blinds</span>
                          <span class="site-navigation__product-icon icon icon--velux"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/vertical-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="vertical blinds">
                          <span itemprop="name">Vertical blinds</span>
                          <span class="site-navigation__product-icon icon icon--vertical"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/roman-blinds/voile-roman-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="voile roman blinds">
                          <span itemprop="name">Voile Roman blinds</span>
                          <span class="site-navigation__product-icon icon icon--voile"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/day-night-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="day &amp; night blinds">
                          <span itemprop="name">Day &amp; Night blinds</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/thermal-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="thermal blinds">
                          <span itemprop="name">Thermal blinds</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/transition-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="transition blinds">
                          <span itemprop="name">Transition blinds</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                                          <li class="site-navigation__item site-navigation__item--tablet-only">
                        <a href="/blinds-range/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="blinds">
                          <span itemprop="name">Browse All Blinds</span>
                          <span class="site-navigation__product-icon icon icon--browse-all-blinds"></span>
                        </a>
                      </li>
                  </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="blinds" data-ga-label="view - room">
                  Room
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/blinds-range/bathroom/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="bathroom blinds">
                          <span itemprop="name">Bathroom blinds</span>
                          <span class="site-navigation__product-icon icon icon--bathroom"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/bedroom/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="bedroom blinds">
                          <span itemprop="name">Bedroom blinds</span>
                          <span class="site-navigation__product-icon icon icon--bedroom"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/childrens-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="children's blinds">
                          <span itemprop="name">Children's blinds</span>
                          <span class="site-navigation__product-icon icon icon--childrens"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/kitchen/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="kitchen blinds">
                          <span itemprop="name">Kitchen blinds</span>
                          <span class="site-navigation__product-icon icon icon--kitchen"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/living-room/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="living room blinds">
                          <span itemprop="name">Living room blinds</span>
                          <span class="site-navigation__product-icon icon icon--living-room"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/bay-windows/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="bay windows">
                          <span itemprop="name">Bay windows</span>
                          <span class="site-navigation__product-icon icon icon--bay-window"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="blinds" data-ga-label="view - colour">
                  Colour
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/blinds-range/black/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="black blinds">
                          <span itemprop="name">Black blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--black"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/blue/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="blue blinds">
                          <span itemprop="name">Blue blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--blue"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/brown/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="brown blinds">
                          <span itemprop="name">Brown blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--brown"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/cream/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="cream blinds">
                          <span itemprop="name">Cream blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--cream"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/green/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="green blinds">
                          <span itemprop="name">Green blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--green"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/grey/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="grey blinds">
                          <span itemprop="name">Grey blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--grey"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/orange/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="orange blinds">
                          <span itemprop="name">Orange blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--orange"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/pink/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="pink blinds">
                          <span itemprop="name">Pink blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--pink"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/purple/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="purple blinds">
                          <span itemprop="name">Purple blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--purple"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/red/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="red blinds">
                          <span itemprop="name">Red blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--red"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/silver/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="silver blinds">
                          <span itemprop="name">Silver blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--silver"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/white/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="white blinds">
                          <span itemprop="name">White blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--white"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/yellow/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="yellow blinds">
                          <span itemprop="name">Yellow blinds</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--yellow"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/floral/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="floral blinds">
                          <span itemprop="name">Floral blinds</span>
                          <span class="site-navigation__product-icon icon-image icon-image--charlotte-beaver"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/patterned/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="patterned blinds">
                          <span itemprop="name">Patterned blinds</span>
                          <span class="site-navigation__product-icon icon-image icon-image--patterned"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/striped/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="striped blinds">
                          <span itemprop="name">Striped blinds</span>
                          <span class="site-navigation__product-icon icon-image icon-image--striped"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/geometric-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="geometric blinds">
                          <span itemprop="name">Geometric blinds</span>
                          <span class="site-navigation__product-icon icon-image icon-image--geometric"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="blinds" data-ga-label="view - inspiration">
                  Inspiration
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/inspiration/blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="blinds inspiration">
                          <span itemprop="name">Blinds inspiration</span>
                          <span class="site-navigation__product-icon icon icon--roller"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/style-guide/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="style guide">
                          <span itemprop="name">Style Guide</span>
                          <span class="site-navigation__product-icon icon icon--styleguide"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/mercury-collection/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="mercury collection">
                          <span itemprop="name">Mercury Collection</span>
                          <span class="site-navigation__product-icon icon-image icon-image--mercury"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/abigail-ahern-collection/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="abigail ahern collection">
                          <span itemprop="name">Abigail Ahern Collection</span>
                          <span class="site-navigation__product-icon icon icon--collections"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/housebeautiful/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="house beautiful collection">
                          <span itemprop="name">House Beautiful collection</span>
                          <span class="site-navigation__product-icon icon-image icon-image--folk-art"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/live-that-style/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="live that style">
                          <span itemprop="name">Live that style</span>
                          <span class="site-navigation__product-icon icon icon--charity"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/blinds/voile-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="voile blinds">
                          <span itemprop="name">Voile blinds</span>
                          <span class="site-navigation__product-icon icon icon--voile-curtains"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="blinds" data-ga-label="view - buying guide">
                  Buying guide
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/inspiration/2018/01/blinds-buying-guide/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="blinds buying guide">
                          <span itemprop="name">Blinds buying guide</span>
                          <span class="site-navigation__product-icon icon icon--faqs"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/blinds-range/your-in-home-appointment/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="your in-home appointment">
                          <span itemprop="name">Your in-home appointment</span>
                          <span class="site-navigation__product-icon icon icon--appointment"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/our-service/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="our service">
                          <span itemprop="name">Our service</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/moving-home/" class="site-navigation__product" itemprop="url" data-ga-action="blinds" data-ga-label="moving home">
                          <span itemprop="name">Moving Home</span>
                          <span class="site-navigation__product-icon icon icon--customers"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
          </ul>
        </div>
      </li>
      <li class="site-navigation__item site-navigation__item--first-level">
        <a href="/curtains/" itemprop="url" class="body site-navigation__link site-navigation__link--has-child" data-ga-action="curtains" data-ga-label="primary" data-site-nav-first-level="">
          <span itemprop="name">Curtains</span>
        </a>
        <div class="site-navigation__child-wrapper">
          <ul class="site-navigation__child  site-navigation__child--second-level ">
            <li class="site-navigation__item">
              <a href="/curtains/" itemprop="url" class="site-navigation__link--large icon--arrow-right--brown" data-ga-action="curtains" data-ga-label="browse all curtains">
                Browse All Curtains
              </a>
            </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="curtains" data-ga-label="view - room">
                  Room
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/curtains/bedroom/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="bedroom curtains">
                          <span itemprop="name">Bedroom curtains</span>
                          <span class="site-navigation__product-icon icon icon--bedroom"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/living-room/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="living room curtains">
                          <span itemprop="name">Living room curtains</span>
                          <span class="site-navigation__product-icon icon icon--living-room"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/dining-room/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="dining room curtains">
                          <span itemprop="name">Dining room curtains</span>
                          <span class="site-navigation__product-icon icon icon--dining-room"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/childrens/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="children's curtains">
                          <span itemprop="name">Children's curtains</span>
                          <span class="site-navigation__product-icon icon icon--childrens"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/kitchen/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="kitchen curtains">
                          <span itemprop="name">Kitchen curtains</span>
                          <span class="site-navigation__product-icon icon icon--kitchen"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/bay-window-curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="bay window curtains">
                          <span itemprop="name">Bay window curtains</span>
                          <span class="site-navigation__product-icon icon icon--bay-window"></span>
                        </a>
                      </li>
                                          <li class="site-navigation__item site-navigation__item--tablet-only">
                        <a href="/curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="curtains">
                          <span itemprop="name">Browse All Curtains</span>
                          <span class="site-navigation__product-icon icon icon--browse-all-curtains"></span>
                        </a>
                      </li>
                  </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="curtains" data-ga-label="view - colour">
                  Colour
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/curtains/blue/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="blue curtains">
                          <span itemprop="name">Blue curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--blue"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/brown/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="brown curtains">
                          <span itemprop="name">Brown curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--brown"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/cream/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="cream curtains">
                          <span itemprop="name">Cream curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--cream"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/green/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="green curtains">
                          <span itemprop="name">Green curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--green"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/orange/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="orange curtains">
                          <span itemprop="name">Orange curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--orange"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/striped/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="striped curtains">
                          <span itemprop="name">Striped curtains</span>
                          <span class="site-navigation__product-icon icon-image icon-image--striped"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/yellow/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="yellow curtains">
                          <span itemprop="name">Yellow curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--yellow"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/grey/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="grey curtains">
                          <span itemprop="name">Grey curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--grey"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/pink/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="pink curtains">
                          <span itemprop="name">Pink curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--pink"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/purple/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="purple curtains">
                          <span itemprop="name">Purple curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--purple"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/red/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="red curtains">
                          <span itemprop="name">Red curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--red"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/silver/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="silver curtains">
                          <span itemprop="name">Silver curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--silver"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/white/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="white curtains">
                          <span itemprop="name">White curtains</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--white"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/patterned/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="patterned curtains">
                          <span itemprop="name">Patterned curtains</span>
                          <span class="site-navigation__product-icon icon-image icon-image--patterned"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/plain/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="plain curtains">
                          <span itemprop="name">Plain curtains</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/floral/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="floral curtains">
                          <span itemprop="name">Floral curtains</span>
                          <span class="site-navigation__product-icon icon-image icon-image--charlotte-beaver"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/geometric-curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="geometric curtains">
                          <span itemprop="name">Geometric curtains</span>
                          <span class="site-navigation__product-icon icon-image icon-image--geometric"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="curtains" data-ga-label="view - curtain types">
                  Curtain types
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/curtains/eyelet-curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="eyelet curtains">
                          <span itemprop="name">Eyelet curtains</span>
                          <span class="site-navigation__product-icon icon icon--eyelet"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/pencil-pleat-curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="pencil pleat curtains">
                          <span itemprop="name">Pencil pleat curtains</span>
                          <span class="site-navigation__product-icon icon icon--pencil-pleat"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/pinch-pleat-curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="pinch pleat curtains">
                          <span itemprop="name">Pinch pleat curtains</span>
                          <span class="site-navigation__product-icon icon icon--pinch-pleat"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/blackout-curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="blackout curtains">
                          <span itemprop="name">Blackout curtains</span>
                          <span class="site-navigation__product-icon icon icon--blackout-curtain"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/wave-header-curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="wave header curtains">
                          <span itemprop="name">Wave header curtains</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/thermal-curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="thermal curtains">
                          <span itemprop="name">Thermal curtains</span>
                          <span class="site-navigation__product-icon icon icon--thermal"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/voile-curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="voile curtains">
                          <span itemprop="name">Voile curtains</span>
                          <span class="site-navigation__product-icon icon icon--voile-curtains"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/curtains/velvet-curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="velvet curtains">
                          <span itemprop="name">Velvet curtains</span>
                          <span class="site-navigation__product-icon icon icon--thermal"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="curtains" data-ga-label="view - inspiration">
                  Inspiration
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/inspiration/curtains/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="curtains inspiration">
                          <span itemprop="name">Curtains inspiration</span>
                          <span class="site-navigation__product-icon icon icon--eyelet"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/curtains/voile/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="voile curtains">
                          <span itemprop="name">Voile curtains</span>
                          <span class="site-navigation__product-icon icon icon--voile"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/mercury-collection/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="mercury collection">
                          <span itemprop="name">Mercury Collection</span>
                          <span class="site-navigation__product-icon icon-image icon-image--mercury"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/abigail-ahern-collection/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="abigail ahern collection">
                          <span itemprop="name">Abigail Ahern Collection</span>
                          <span class="site-navigation__product-icon icon icon--collections"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/housebeautiful/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="house beautiful collection">
                          <span itemprop="name">House Beautiful collection</span>
                          <span class="site-navigation__product-icon icon-image icon-image--folk-art"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/live-that-style/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="live that style">
                          <span itemprop="name">Live that style</span>
                          <span class="site-navigation__product-icon icon icon--charity"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="curtains" data-ga-label="view - buying guide">
                  Buying guide
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/curtains/your-in-home-appointment/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="your in-home appointment">
                          <span itemprop="name">Your in-home appointment</span>
                          <span class="site-navigation__product-icon icon icon--appointment"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/our-service/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="our service">
                          <span itemprop="name">Our service</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/2017/06/curtains-buying-guide/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="curtains buying guide">
                          <span itemprop="name">Curtains buying guide</span>
                          <span class="site-navigation__product-icon icon icon--faqs"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/moving-home/" class="site-navigation__product" itemprop="url" data-ga-action="curtains" data-ga-label="moving home">
                          <span itemprop="name">Moving Home</span>
                          <span class="site-navigation__product-icon icon icon--customers"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
          </ul>
        </div>
      </li>
      <li class="site-navigation__item site-navigation__item--first-level">
        <a href="/shutters-range/" itemprop="url" class="body site-navigation__link site-navigation__link--has-child" data-ga-action="shutters" data-ga-label="primary" data-site-nav-first-level="">
          <span itemprop="name">Shutters</span>
        </a>
        <div class="site-navigation__child-wrapper">
          <ul class="site-navigation__child  site-navigation__child--second-level ">
            <li class="site-navigation__item">
              <a href="/shutters-range/" itemprop="url" class="site-navigation__link--large icon--arrow-right--brown" data-ga-action="shutters" data-ga-label="browse all shutters">
                Browse All Shutters
              </a>
            </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="shutters" data-ga-label="view - our shutters range">
                  Our shutters range
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/shutters-range/full-height-shutters/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="full height shutters">
                          <span itemprop="name">Full height shutters</span>
                          <span class="site-navigation__product-icon icon icon--full-height"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/tier-on-tier-shutters/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="tier on tier shutters">
                          <span itemprop="name">Tier on tier shutters</span>
                          <span class="site-navigation__product-icon icon icon--tier-on-tier"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/cafe-style-shutters/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="cafe style shutters">
                          <span itemprop="name">Cafe style shutters</span>
                          <span class="site-navigation__product-icon icon icon--cafe-style"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/tracked-shutters/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="tracked shutters">
                          <span itemprop="name">Tracked shutters</span>
                          <span class="site-navigation__product-icon icon icon--tracked"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/shaped-shutters/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="shaped shutters">
                          <span itemprop="name">Shaped shutters</span>
                          <span class="site-navigation__product-icon icon icon--shaped"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/solid-shutters/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="solid shutters">
                          <span itemprop="name">Solid shutters</span>
                          <span class="site-navigation__product-icon icon icon--solid"></span>
                        </a>
                      </li>
                                          <li class="site-navigation__item site-navigation__item--tablet-only">
                        <a href="/shutters-range/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="shutters">
                          <span itemprop="name">Browse All Shutters</span>
                          <span class="site-navigation__product-icon icon icon--browse-all-shutters"></span>
                        </a>
                      </li>
                  </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="shutters" data-ga-label="view - colour">
                  Colour
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/shutters-range/white/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="white shutters">
                          <span itemprop="name">White shutters</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--white"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/grey/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="grey shutters">
                          <span itemprop="name">Grey shutters</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--grey"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/cream/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="cream shutters">
                          <span itemprop="name">Cream shutters</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--cream"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/brown/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="brown shutters">
                          <span itemprop="name">Brown shutters</span>
                          <span class="site-navigation__product-icon icon-gradient icon-gradient--brown"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="shutters" data-ga-label="view - room">
                  Room
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/shutters-range/bathroom/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="bathroom shutters">
                          <span itemprop="name">Bathroom shutters</span>
                          <span class="site-navigation__product-icon icon icon--bathroom"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/living-room/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="living room shutters">
                          <span itemprop="name">Living room shutters</span>
                          <span class="site-navigation__product-icon icon icon--living-room"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/bedroom/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="bedroom shutters">
                          <span itemprop="name">Bedroom shutters</span>
                          <span class="site-navigation__product-icon icon icon--bedroom"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/dining-room/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="dining room shutters">
                          <span itemprop="name">Dining room shutters</span>
                          <span class="site-navigation__product-icon icon icon--dining-room"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="shutters" data-ga-label="view - inspiration">
                  Inspiration
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/inspiration/shutters/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="shutters inspiration">
                          <span itemprop="name">Shutters inspiration</span>
                          <span class="site-navigation__product-icon icon icon--tier-on-tier"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/housebeautiful/atmosphere/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="house beautiful shutters collection">
                          <span itemprop="name">House Beautiful Shutters collection</span>
                          <span class="site-navigation__product-icon icon-image icon-image--house-beautiful-shutter"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/in-home-visualisation-tool/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="in-home visualisation app">
                          <span itemprop="name">In-home visualisation app</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="shutters" data-ga-label="view - buying guide">
                  Buying Guide
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/shutters-range/our-shutter-styles/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="our shutter styles">
                          <span itemprop="name">Our shutter styles</span>
                          <span class="site-navigation__product-icon icon icon--full-height"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/the-hillarys-difference/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="your in-home appointment">
                          <span itemprop="name">Your in-home appointment</span>
                          <span class="site-navigation__product-icon icon icon--appointment"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/shutters-range/tips-and-advice/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="tips and advice">
                          <span itemprop="name">Tips and advice</span>
                          <span class="site-navigation__product-icon icon icon--faqs"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/moving-home/" class="site-navigation__product" itemprop="url" data-ga-action="shutters" data-ga-label="moving home">
                          <span itemprop="name">Moving Home</span>
                          <span class="site-navigation__product-icon icon icon--customers"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
          </ul>
        </div>
      </li>
      <li class="site-navigation__item site-navigation__item--first-level">
        <a href="/awnings/" itemprop="url" class="body site-navigation__link site-navigation__link--has-child" data-ga-action="awnings" data-ga-label="primary" data-site-nav-first-level="">
          <span itemprop="name">Awnings</span>
        </a>
        <div class="site-navigation__child-wrapper">
          <ul class="site-navigation__child  site-navigation__child--second-level ">
            <li class="site-navigation__item">
              <a href="/awnings/" itemprop="url" class="site-navigation__link--large icon--arrow-right--brown" data-ga-action="awnings" data-ga-label="browse all awnings">
                Browse All Awnings
              </a>
            </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="awnings" data-ga-label="view - our awnings range">
                  Our Awnings Range
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/awnings/" class="site-navigation__product" itemprop="url" data-ga-action="awnings" data-ga-label="awnings">
                          <span itemprop="name">Awnings</span>
                          <span class="site-navigation__product-icon icon icon--awning"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/awnings/" class="site-navigation__product" itemprop="url" data-ga-action="awnings" data-ga-label="awnings inspiration">
                          <span itemprop="name">Awnings inspiration</span>
                          <span class="site-navigation__product-icon icon icon--inspiration"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/our-service/" class="site-navigation__product" itemprop="url" data-ga-action="awnings" data-ga-label="our service">
                          <span itemprop="name">Our service</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/awnings/your-in-home-appointment/" class="site-navigation__product" itemprop="url" data-ga-action="awnings" data-ga-label="your in-home appointment">
                          <span itemprop="name">Your in-home appointment</span>
                          <span class="site-navigation__product-icon icon icon--appointment"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
          </ul>
        </div>
      </li>
      <li class="site-navigation__item site-navigation__item--first-level">
        <a href="/conservatory-blinds/" itemprop="url" class="body site-navigation__link site-navigation__link--has-child" data-ga-action="conservatory blinds" data-ga-label="primary" data-site-nav-first-level="">
          <span itemprop="name">Conservatory blinds</span>
        </a>
        <div class="site-navigation__child-wrapper">
          <ul class="site-navigation__child  site-navigation__child--second-level ">
            <li class="site-navigation__item">
              <a href="/conservatory-blinds/" itemprop="url" class="site-navigation__link--large icon--arrow-right--brown" data-ga-action="conservatory blinds" data-ga-label="browse all conservatory blinds">
                Browse All Conservatory blinds
              </a>
            </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="conservatory blinds" data-ga-label="view - type">
                  Type
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/conservatory-blinds/blinds-for-the-roof/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="conservatory blinds for the roof">
                          <span itemprop="name">Conservatory blinds for the roof</span>
                          <span class="site-navigation__product-icon icon icon--conservatory-roof"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/conservatory-blinds/blinds-for-the-sides/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="conservatory blinds for the sides">
                          <span itemprop="name">Conservatory blinds for the sides</span>
                          <span class="site-navigation__product-icon icon icon--conservatory-side"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/conservatory-blinds/specialist-pleated-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="specialist pleated blinds">
                          <span itemprop="name">Specialist Pleated blinds</span>
                          <span class="site-navigation__product-icon icon icon--pleated"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/conservatory-blinds/perfect-fit/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="perfect fit">
                          <span itemprop="name">Perfect fit</span>
                          <span class="site-navigation__product-icon icon icon--perfectfit"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/conservatory-blinds/vertical-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="vertical blinds">
                          <span itemprop="name">Vertical blinds</span>
                          <span class="site-navigation__product-icon icon icon--vertical"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/conservatory-blinds/venetian-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="venetian blinds">
                          <span itemprop="name">Venetian blinds</span>
                          <span class="site-navigation__product-icon icon icon--venetian"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/conservatory-blinds/wooden-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="wooden blinds">
                          <span itemprop="name">Wooden blinds</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/conservatory-blinds/roman-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="roman blinds">
                          <span itemprop="name">Roman blinds</span>
                          <span class="site-navigation__product-icon icon icon--roman"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/conservatory-blinds/roller-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="roller blinds">
                          <span itemprop="name">Roller blinds</span>
                          <span class="site-navigation__product-icon icon icon--roller"></span>
                        </a>
                      </li>
                                          <li class="site-navigation__item site-navigation__item--tablet-only">
                        <a href="/conservatory-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="conservatory blinds">
                          <span itemprop="name">Browse All Conservatory blinds</span>
                          <span class="site-navigation__product-icon icon icon--browse-all-conservatory blinds"></span>
                        </a>
                      </li>
                  </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="conservatory blinds" data-ga-label="view - buying guide">
                  Buying Guide
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/conservatory-blinds/tips-and-advice/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="tips and advice">
                          <span itemprop="name">Tips and advice</span>
                          <span class="site-navigation__product-icon icon icon--choosing"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/2016/01/how-to-clean-conservatory-side-blinds/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="how to clean conservatory side blinds">
                          <span itemprop="name">How to clean conservatory side blinds</span>
                          <span class="site-navigation__product-icon icon icon--cleaning"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/our-service/" class="site-navigation__product" itemprop="url" data-ga-action="conservatory blinds" data-ga-label="our service">
                          <span itemprop="name">Our service</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
          </ul>
        </div>
      </li>
      <li class="site-navigation__item site-navigation__item--first-level">
        <a href="/inspiration/" itemprop="url" class="body site-navigation__link site-navigation__link--has-child" data-ga-action="inspiration" data-ga-label="primary" data-site-nav-first-level="">
          <span itemprop="name">Inspiration</span>
        </a>
        <div class="site-navigation__child-wrapper">
          <ul class="site-navigation__child  site-navigation__child--second-level ">
            <li class="site-navigation__item">
              <a href="/inspiration/" itemprop="url" class="site-navigation__link--large icon--arrow-right--brown" data-ga-action="inspiration" data-ga-label="browse all inspiration">
                Browse All Inspiration
              </a>
            </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="inspiration" data-ga-label="view - take a look at ">
                  Take a look at 
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/inspiration/latest-trends/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="latest trends">
                          <span itemprop="name">Latest trends</span>
                          <span class="site-navigation__product-icon icon icon--collections"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/new-collections/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="new collections">
                          <span itemprop="name">New Collections</span>
                          <span class="site-navigation__product-icon icon icon--styleguide"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/life-styles/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="hillarys lifestyles">
                          <span itemprop="name">Hillarys LifeStyles</span>
                          <span class="site-navigation__product-icon icon icon--lifestyle"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/meet-our-customers/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="real homes">
                          <span itemprop="name">Real Homes</span>
                          <span class="site-navigation__product-icon icon icon--customers"></span>
                        </a>
                      </li>
                                          <li class="site-navigation__item site-navigation__item--tablet-only">
                        <a href="/inspiration/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="inspiration">
                          <span itemprop="name">Browse All Inspiration</span>
                          <span class="site-navigation__product-icon icon icon--browse-all-inspiration"></span>
                        </a>
                      </li>
                  </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="inspiration" data-ga-label="view - room">
                  Room
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/inspiration/room/bathroom/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="bathroom">
                          <span itemprop="name">Bathroom</span>
                          <span class="site-navigation__product-icon icon icon--bathroom"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/room/bedroom/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="bedroom">
                          <span itemprop="name">Bedroom</span>
                          <span class="site-navigation__product-icon icon icon--bedroom"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/room/nursery/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="nursery">
                          <span itemprop="name">Nursery</span>
                          <span class="site-navigation__product-icon icon icon--childrens"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/room/kitchen/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="kitchen">
                          <span itemprop="name">Kitchen</span>
                          <span class="site-navigation__product-icon icon icon--kitchen"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/room/living-room/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="living room">
                          <span itemprop="name">Living room</span>
                          <span class="site-navigation__product-icon icon icon--living-room"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/bay-windows/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="bay windows">
                          <span itemprop="name">Bay windows</span>
                          <span class="site-navigation__product-icon icon icon--bay-window"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/conservatories/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="conservatories">
                          <span itemprop="name">Conservatories</span>
                          <span class="site-navigation__product-icon icon icon--conservatory"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="inspiration" data-ga-label="view - type">
                  Type
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/inspiration/blinds-type/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="blinds inspiration">
                          <span itemprop="name">Blinds inspiration</span>
                          <span class="site-navigation__product-icon icon icon--roller"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/curtains/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="curtains inspiration">
                          <span itemprop="name">Curtains inspiration</span>
                          <span class="site-navigation__product-icon icon icon--eyelet"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/shutters/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="shutters inspiration">
                          <span itemprop="name">Shutters inspiration</span>
                          <span class="site-navigation__product-icon icon icon--tier-on-tier"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/inspiration/awnings/" class="site-navigation__product" itemprop="url" data-ga-action="inspiration" data-ga-label="awnings inspiration">
                          <span itemprop="name">Awnings inspiration</span>
                          <span class="site-navigation__product-icon icon icon--inspiration"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
          </ul>
        </div>
      </li>
      <li class="site-navigation__item site-navigation__item--first-level">
        <a href="/about-hillarys/" itemprop="url" class="body site-navigation__link site-navigation__link--has-child" data-ga-action="about us" data-ga-label="primary" data-site-nav-first-level="">
          <span itemprop="name">About us</span>
        </a>
        <div class="site-navigation__child-wrapper">
          <ul class="site-navigation__child  site-navigation__child--second-level ">
            <li class="site-navigation__item">
              <a href="/about-hillarys/" itemprop="url" class="site-navigation__link--large icon--arrow-right--brown" data-ga-action="about us" data-ga-label="browse all about us">
                Browse All About us
              </a>
            </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="about us" data-ga-label="view - careers">
                  Careers
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/careers/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="careers">
                          <span itemprop="name">Careers</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/careers/become-an-advisor/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="become an advisor">
                          <span itemprop="name">Become an advisor</span>
                          <span class="site-navigation__product-icon icon icon--appointment"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/careers/head-office/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="head office roles">
                          <span itemprop="name">Head office roles</span>
                          <span class="site-navigation__product-icon icon icon--careers"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/careers/company-values/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="company values">
                          <span itemprop="name">Company values</span>
                          <span class="site-navigation__product-icon icon icon--charity"></span>
                        </a>
                      </li>
                                          <li class="site-navigation__item site-navigation__item--tablet-only">
                        <a href="/about-hillarys/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="about us">
                          <span itemprop="name">Browse All About us</span>
                          <span class="site-navigation__product-icon icon icon--browse-all-about us"></span>
                        </a>
                      </li>
                  </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="about us" data-ga-label="view - our company">
                  Our Company
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/history-of-hillarys/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="history">
                          <span itemprop="name">History</span>
                          <span class="site-navigation__product-icon icon icon--history"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/press-media/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="press &amp; media">
                          <span itemprop="name">Press &amp; Media</span>
                          <span class="site-navigation__product-icon icon icon--press-media"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/committed-to-charity/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="charity">
                          <span itemprop="name">Charity</span>
                          <span class="site-navigation__product-icon icon icon--charity"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
              <li class="site-navigation__item site-navigation__item--haschild ">
                <button class="site-navigation__heading" data-site-nav-second-level="" data-ga-action="about us" data-ga-label="view - about us">
                  About us
                  <span class="site-navigation__heading-icon"></span>
                </button>
                <div class="site-navigation__product-list-wrapper">
                  <ul class="site-navigation__product-list">
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/our-service/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="our service">
                          <span itemprop="name">Our service</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/your-local-hillarys/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="find your local advisor">
                          <span itemprop="name">Find your local advisor</span>
                          <span class="site-navigation__product-icon icon icon--hillarys"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/reviews/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="reviews">
                          <span itemprop="name">Reviews</span>
                          <span class="site-navigation__product-icon icon icon--review"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/faqs/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="faqs">
                          <span itemprop="name">FAQs</span>
                          <span class="site-navigation__product-icon icon icon--faqs"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/visit-us-in-your-area/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="in your area">
                          <span itemprop="name">In your area</span>
                          <span class="site-navigation__product-icon icon icon--press-media"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/product-testing/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="product testing">
                          <span itemprop="name">Product Testing</span>
                          <span class="site-navigation__product-icon icon icon--product-testing"></span>
                        </a>
                      </li>
                      <li class="site-navigation__item">
                        <a href="/about-hillarys/made-in-britain/" class="site-navigation__product" itemprop="url" data-ga-action="about us" data-ga-label="made in the uk">
                          <span itemprop="name">Made in the UK</span>
                          <span class="site-navigation__product-icon icon icon--made-in-uk"></span>
                        </a>
                      </li>
                                      </ul>
                </div>
              </li>
          </ul>
        </div>
      </li>
    <li class="site-navigation__item site-navigation__item--first-level  site-navigation__item--hide-on-desktop">
      <a href="/arrange-an-appointment/" class="site-navigation__link" data-site-nav-first-level="" data-ga-action="request an appointment" data-ga-label="request an appointment">
        Request an Appointment
      </a>
    </li>
    <li class="site-navigation__item site-navigation__item--first-level  site-navigation__item--hide-on-desktop">
      <a href="/request-a-brochure/" class="site-navigation__link" data-site-nav-first-level="" data-ga-action="request an brochure" data-ga-label="request an brochure">
        Request a Brochure
      </a>
    </li>
    <li class="site-navigation__item site-navigation__item--first-level site-navigation__item--search">
      <span class="body site-navigation__link  site-navigation__link--has-child site-navigation__link--search " data-site-nav-first-level="">
        Search
        <span class="site-navigation__link-icon site-navigation__link-icon--search"></span>
      </span>
      <div class="site-navigation__child-wrapper">
        <div class="site-navigation__child">
          <div class="site-navigation__item site-navigation__item--haschild site-navigation__item--search-section">
            <form class="site-search-new" id="openSearch" method="GET" autocomplete="off" action="/search/" data-search-form="">
  <fieldset class="site-search-new__container">
    <legend class="body u-visuallyhidden">Search</legend>
    <div class="site-search-new__wrapper">
      <div class="site-search-new__input-wrapper">
        <label for="query" class="u-visuallyhidden">Search</label>
        <input class="site-search-new__input body" type="search" name="query" id="query" maxlength="200" placeholder="Search Products..." data-search-input="">
        <button class="site-search-new__btn" type="submit" aria-label="Search">
          Search
        </button>
      </div>
    </div>
  </fieldset>
</form>

          </div>
        </div>
      </div>
    </li>
    <li class="site-navigation__item site-navigation__item--first-level  site-navigation__item--hide-on-desktop site-navigation__footer">



      <div class="site-navigation__link site-navigation__phone-wrapper">
        <span class="site-navigation__call-now-text">
          Call Now - Lines open until 9pm
        </span>
        <em class="site-navigation__phone">
          <svg class="site-navigation__phone-icon" width="32" height="32" viewBox="0 0 32 32">
            <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#phone"></use>
          </svg>
          <a class="site-navigation__phone-number InfinityNumber" href="tel:08082397598">0808 239 7598</a>
        </em>
      </div>
    </li>
  </ul>
  <button class="site-header__button site-header__nav-hamburger" data-menu-toggle="">
    <span class="site-header__nav-hamburger-text">Open Menu</span>
    <svg class="site-header__nav-hamburger-icon">
      <use xlink:href="/build/global/svg-sprites/default-sprite.svg?cb=8b4f9f5d063900f8bf09eeecc2383ed3#hamburger"></use>
    </svg>
  </button>
</nav>

    </header>
