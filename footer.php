    </main>
    <?php // Close Main ?>

    <footer class="l-footer">
        <?php include( 'assets/img/tmp/logo.svg' ); ?>

        <p>‘Commuter Calories’ comes courtesy of Hillarys, the interiors experts inspiring the nation with beautiful <a href="https://www.hillarys.co.uk/blinds-range/" target="_blank">blinds</a>, shutters and&nbsp;curtains.</p>

        <div class="l-footer__sources">
            <a href="#">Sources +</a>

            <ul>
                <li>en.tempo.co</li>
                <li>www.howmany.wiki</li>
                <li>www.thedailymeal.com</li>
                <li>www.fitbit.com</li>
                <li>www.nhs.uk</li>
            </ul>
        </div>
    </footer>

    </div>
    <?php // Close Wrapper ?>
</div>
<?php // Close App ?>

<?php if ($brand == 'Hillarys') { ?>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-3874421-1', 'auto');
    ga('send', 'pageview');
</script>
<?php } else { ?>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-2548785-1', 'auto');
    ga('send', 'pageview');
</script>
<?php } ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>

    <?php if( strstr( $_SERVER['SERVER_NAME'], "localhost" ) === "localhost" ) { ?>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.core.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.interchange.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.util.triggers.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.util.mediaQuery.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.util.timerAndImageLoader.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.util.keyboard.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.util.motion.js"></script>
        <script src="node_modules/jquery-match-height/dist/jquery.matchHeight.js"></script>
        <script src="node_modules/flickity/dist/flickity.pkgd.js"></script>
        <script src="node_modules/lazysizes/lazysizes.js"></script>
        <script src="node_modules/lazysizes/plugins/object-fit/ls.object-fit.js"></script>
        <script src="node_modules/jquery-inview/jquery.inview.js"></script>
        <script src="node_modules/jquery-sticky/jquery.sticky.js"></script>
        <script src="node_modules/object-fit-videos/dist/object-fit-videos.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.3.3/rangeslider.min.js"></script>
        <script src="assets/js/libs/modernizr.js"></script>
        <script src="assets/js/libs/velocity.js"></script>
        <script src="assets/js/libs/select2.js"></script>
        <script src="assets/js/dist/global.js"></script>
    <?php } else { ?>
        <script src="assets/js/main.js?<?php echo filemtime( __DIR__ . '/assets/js/main.js' ); ?>"></script>
    <?php } ?>
</body>
</html>