<?php

// Template Parts
require_once( 'inc/classes/class-lazy-image.php' );
require_once( 'inc/classes/class-modules.php' );
// require_once( 'inc/classes/class-slideshows.php' );

?>
<!DOCTYPE html>
<html lang="en">
<head>

<?php

// Start Meta
include( 'partials/meta.php' );
// End Meta

?>

<!-- Hillarys stylesheet -->
<link rel="stylesheet" href="//static.hillarys.co.uk/build/styles/main.css" media="all">

<!-- Custom stylesheet -->
<link rel="stylesheet" href="/assets/css/main.css?<?php echo filemtime( __DIR__ . '/assets/css/main.css' ); ?>" type="text/css" media="all" />

</head>

<body>

    <div class="hide"><?php include( 'assets/img/icons/svg-symbols.svg' ); ?></div>

    <?php // Open App ?>
    <div id="app" class="site">

        <?php // Open Wrapper ?>
        <div id="page_wrapper">

        <?php

        // Start Header
        // include( 'partials/header.php' );
        // End Header

        ?>

        <header class="l-header">
            <div class="l-header__share">
                <span>Share this</span>

                <?php

                // Add in the correct share information.

                ?>

                <ul>
                    <li>
                        <a href="https://twitter.com/intent/tweet?text=Discover%20if%20working%20from%20home%20has%20affected%20your%20calorie%20intake%20with%20the%20new%20online%20tool,%20Commuter%20Calories.%20https://www.hillarys.co.uk/static/commuter-calories/" target="_blank"><?php include( 'assets/img/tmp/twitter.svg' ); ?></a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.hillarys.co.uk/static/commuter-calories/" target="_blank"><?php include( 'assets/img/tmp/facebook.svg' ); ?></a>
                    </li>
                    <li>
                        <a href="whatsapp://send?text=Discover%20if%20working%20from%20home%20has%20affected%20your%20calorie%20intake%20with%20the%20new%20online%20tool,%20Commuter%20Calories.%20https://www.hillarys.co.uk/static/commuter-calories/" target="_blank"><?php include( 'assets/img/tmp/whatsapp.svg' ); ?></a>
                    </li>
                </ul>
            </div>
        </header>

        <?php // Open Main ?>
        <main class="l-main">