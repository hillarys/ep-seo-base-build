<?php

$brand = 'Hillarys'; // Hillarys or Thomas Sanderson
$static_url = 'https://www.hillarys.co.uk/static/commuter-calories';
$static_url_ie = 'https://www.hillarys.ie/static/commuter-calories';
$meta_title = 'Example Meta Title | Hillarys';
$meta_description = 'Example Meta Description';
$social_title = 'Example Social Title | Hillarys';
$social_description = 'Example Social Description';


include( 'header.php' ); ?>

<section class="c-landing">
    <div class="container">
        <div class="c-landing__content">
            <h1 class="c-landing__logo"><?php include( 'assets/img/tmp/logo-landing.svg' ); ?> <span class="u-visually-hidden">Commuter Calories</span></h1>

            <p>Following the recent disruptions to our lives, many of us are now working from home and no longer commuting. So we thought it would be interesting to see if we’re consuming more or less calories now our habits have changed.</p>

            <a href="#" class="c-button c-button--arrow">Let's Go <?php include( 'assets/img/tmp/arrow.svg' ); ?></a>
        </div>
    </div>
</section>

<?php include( 'footer.php' ); ?>